package com.egineering.bootsecrets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Map;

/**
 *
 */
@RestController
public class ExampleController {

    private final Environment environment;
    private final SecretsReader secretsReader;

    @Autowired
    public ExampleController(Environment environment, SecretsReader secretsReader){
        this.environment = environment;
        this.secretsReader = secretsReader;
    }

    @GetMapping(path="/api/time")
    public LocalDateTime getTime(){
        return LocalDateTime.now();
    }

    @GetMapping(path="/api/spillsecrets")
    public Map<String,String> getSpilledSecrets(){
        return secretsReader.getSecrets();
    }

    @GetMapping(path="/api/secret/{secret}")
    public String getSecret(@PathVariable("secret") String secret){
        return secretsReader.getSecret(secret);
    }
}
