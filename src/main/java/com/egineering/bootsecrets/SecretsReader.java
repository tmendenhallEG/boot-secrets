package com.egineering.bootsecrets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.yaml.snakeyaml.Yaml;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class SecretsReader {

    private static Logger logger = LoggerFactory.getLogger(SecretsReader.class);
    private static Environment environment;
    private Map<String,String> secrets;

    public SecretsReader(Environment environment) {
        this.environment = environment;
        this.secrets = new HashMap<>();

        //read secrets from the secrets file
        String secretsFile = environment.getProperty("secrets.file","/run/secrets/bootsecrets.yaml");

        try(BufferedReader br = new BufferedReader(new FileReader(secretsFile))) {

            Yaml yaml = new Yaml();
            secrets = (Map<String,String>)yaml.load(br);

        } catch (FileNotFoundException e) {
            logger.error("Secrets file not found",e);
        } catch (IOException e) {
            logger.error("Cannot read secrets file",e);
        }

    }

    public Map<String, String> getSecrets() {
        return Collections.unmodifiableMap(secrets);
    }

    public String getSecret(final String selectedSecret){
        return this.secrets.get(selectedSecret);
    }
}
