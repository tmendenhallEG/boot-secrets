package com.egineering.bootsecrets.config;

import com.egineering.bootsecrets.SecretsReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;



/**
 *
 */
@Configuration
public class SecretsSource {

    @Bean
    @Autowired
    public SecretsReader getSecretsReader(Environment environment){
        return new SecretsReader(environment);
    }

}
