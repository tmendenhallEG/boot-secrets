package com.egineering.bootsecrets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication()
public class BootSecretsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootSecretsApplication.class, args);
	}
}
