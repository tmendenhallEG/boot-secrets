FROM openjdk:8-jre-alpine
VOLUME /tmp
ADD app.jar .
EXPOSE 8080
RUN sh -c 'touch /app.jar'
ENTRYPOINT java -Xmx128M -Djava.security.egd=file:/dev/./urandom -jar /app.jar 2>&1