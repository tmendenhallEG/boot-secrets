# Boot-Secrets

Test project for reading Docker Swarm secrets as a yaml file

## Note: only tested with a single level yaml file mapped to a
<String,String> map

## Secrets location
Pass to Spring as command line parameter or Environment Variable
```bash
--secrets.file=/run/secrets/yourfile
or

SECRETS_FILE /run/secrets/yourfile
```

the default location from the application.yaml is for
local execution. See the example bootsecrets.yaml

which would become a Swarm secret (you don't need to include the extension)
k8s is next
